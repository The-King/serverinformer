package me.kinggoesgaming.serverinformer;

import me.kinggoesgaming.serverinformer.cmds.CMDWebsite;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.logging.Logger;

/**
 * @author KingGoesGaming
 *
 *         The main class of ServerInformer
 */
public class ServerInformer extends JavaPlugin {

    public static ServerInformer plugin;
    public final Logger logger = Logger.getLogger("Minecraft");

    @Override
    public void onEnable() {
        PluginDescriptionFile pdfFile = this.getDescription();

        // Register commands
        // Admin command
        this.getCommand("serverinformer").setExecutor(new me.kinggoesgaming.serverinformer.cmds.CMDServerInformer(this));
        this.getCommand("informer").setExecutor(new me.kinggoesgaming.serverinformer.cmds.CMDServerInformer(this));
        this.getCommand("si").setExecutor(new me.kinggoesgaming.serverinformer.cmds.CMDServerInformer(this));
        // Website command
        this.getCommand("website").setExecutor(new CMDWebsite(this));

        // Plugin enabled and working
        logger.info(pdfFile.getName() + " v" + pdfFile.getVersion() + " has been enabled!");
    }

    @Override
    public void onDisable() {
        PluginDescriptionFile pdfFile = this.getDescription();

        // Plugin disabled
        logger.info(pdfFile.getName() + " has been disabled!");
    }
}
