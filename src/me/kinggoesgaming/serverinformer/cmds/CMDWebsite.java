package me.kinggoesgaming.serverinformer.cmds;

import me.kinggoesgaming.serverinformer.ServerInformer;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * @author KingGoesGaming
 *         <p>
 *         Class for /website command
 */
public class CMDWebsite implements CommandExecutor {
    private final ServerInformer plugin;

    public CMDWebsite(ServerInformer plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if (cmd.getName().equalsIgnoreCase("website")) {

            String website;

            if (sender instanceof Player) {
                Player player = (Player) sender;
                if (!player.hasPermission("si.player.website")) {
                    // TODO connect config no perm message
                    player.sendMessage("No Permission");
                    return true;
                }
                player.sendMessage(ChatColor.GREEN + "Website placeholder");
                return true;
            }
            sender.sendMessage(ChatColor.AQUA + "Website placeholder");
            return true;
        }

        return false;
    }
}
