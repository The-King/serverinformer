package me.kinggoesgaming.serverinformer.cmds;

import me.kinggoesgaming.serverinformer.ServerInformer;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginDescriptionFile;

/**
 * @author KingGoesGaming
 *
 * Class for /serverinformer command
 */
public class CMDServerInformer implements CommandExecutor {
    private final ServerInformer plugin;

    public CMDServerInformer(ServerInformer plugin) {
        this.plugin = plugin;
    }


    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if ((cmd.getName().equalsIgnoreCase("serverinformer")) || (cmd.getName().equalsIgnoreCase("si")) || (cmd.getName().equalsIgnoreCase("informer"))) {
            PluginDescriptionFile pdfFile = plugin.getDescription();

            if (sender instanceof Player) {
                Player player = (Player) sender;
                if (!player.hasPermission("si.admin")) {
                    // TODO connect config no perm message
                    player.sendMessage("No Permission");
                    return true;
                }
                player.sendMessage(ChatColor.GREEN + pdfFile.getName() + ": " + pdfFile.getDescription());
                player.sendMessage(ChatColor.GREEN + "Version: " + pdfFile.getVersion());
                player.sendMessage(ChatColor.GREEN + "Author: " + pdfFile.getAuthors());
                player.sendMessage(ChatColor.GREEN + "Website: " + pdfFile.getWebsite()); // TODO Add Spigot Website Link After Publishing First Version
            }
            sender.sendMessage(ChatColor.AQUA + pdfFile.getName() + ": " + pdfFile.getDescription());
            sender.sendMessage(ChatColor.AQUA + "Version: " + pdfFile.getVersion());
            sender.sendMessage(ChatColor.AQUA + "Author: " + pdfFile.getAuthors());
            sender.sendMessage(ChatColor.AQUA + "Website: " + pdfFile.getWebsite()); // TODO Add Spigot Website Link After Publishing First Version
            return true;
        }
        return false;
    }
}
